import { Injectable } from '@angular/core';
import {Cars} from '../cars';
import {Car} from '../car';
import {Observable} from 'rxjs/';
import {of} from 'rxjs/';


@Injectable({
  providedIn: 'root'
})
export class CarService {

  getCars():Observable<Car[]>{
    return of(Cars);
  }

  getCar(id:number):Observable<Car>{
    return of(Cars.find(car => car.id === id));
  }
  constructor() { }

}

import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {CarService} from '../services/car.service';
import {Car} from '../car';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
@Input() car : Car;

  constructor(private route:ActivatedRoute,private location:Location,private carService:CarService) { }

  ngOnInit(): void {
    this.getCar();
  }

  getCar():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.carService.getCar(id)
      .subscribe(car => this.car = car);
  }

  goBack():void{
    this.location.back();
  }

}

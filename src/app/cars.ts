import {Car} from './car';

export const Cars:Car[]=[
    {
        id : 1,
        make : 'BMW',
        model : 'B01',
        year : '2016',
        description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        image : 'assets/images/bmw.jpg'
    },
    {
        id : 2,
        make : 'Audi',
        model : 'A01',
        year : '2015',
        description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        image : 'assets/images/audi.jpg'
    },
    {
        id : 3,
        make : 'FIAT',
        model : 'J30',
        year : '2018',
        description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        image : 'assets/images/fiat.jpg'
    }
];
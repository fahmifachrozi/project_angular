export class Car {
    id : number;
    make : string;
    model : string;
    year : string;
    description : string;
    image : string;
}
import { Component, OnInit } from '@angular/core';
import {Car} from '../car';
import {CarService} from '../services/car.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  cars:Car[];

  constructor(private carService:CarService) { }

  getCar():void{
    this.carService.getCars()
      .subscribe(cars => this.cars = cars);
  }
  ngOnInit(): void {
    this.getCar();
  }

}
